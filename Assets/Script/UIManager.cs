using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    private Text _scoreText;
    [SerializeField]
    private Text _gameOverText;
    [SerializeField]
    private Text _restartText;

    [SerializeField]
    private Sprite[] _lifesSprite;

    [SerializeField]
    private Image _lifesImg;

    private GameManager _gameManager;

    // Start is called before the first frame update
    void Start()
    {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        _lifesImg.gameObject.SetActive(true);
        _scoreText.text = "Score: 0";
        _gameOverText.gameObject.SetActive(false);
        _restartText.gameObject.SetActive(false);

        if(_gameManager == null)
        {
            Debug.LogError("GameManager is null.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateScore(int score)
    {
        _scoreText.text = "Score: " + score;
    }

    public void UpdateLifes(int playerLife)
    {
        if (playerLife != -1)
        {
            _lifesImg.sprite = _lifesSprite[playerLife];
        }
        else
        {
            StartCoroutine(GameOverFlickerRoutine());
            _lifesImg.gameObject.SetActive(false);
            _gameOverText.gameObject.SetActive(true);
            _restartText.text = "Press R to restart or ESC to return to Menu!";
            _restartText.gameObject.SetActive(true);
            _gameManager.GameOver();

        }
    }

    IEnumerator GameOverFlickerRoutine()
    {
        while (true)
        {
            _gameOverText.text = "GAME OVER";
            yield return new WaitForSeconds(0.5f);
            _gameOverText.text = "";
            yield return new WaitForSeconds(0.5f);
        }
    }

    public void SetPauseUI(bool pause)
    {
        if(pause == false)
        {
            _restartText.gameObject.SetActive(true);
            _restartText.text = "Game in pause, press P to continue!!";
        }
        else
        {
            _restartText.gameObject.SetActive(false);
        }

    }
}
