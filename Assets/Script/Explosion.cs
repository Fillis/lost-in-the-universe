using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(EnemyDeath());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator EnemyDeath()
    {
        yield return new WaitForSeconds(0.3f);
        Destroy(this.gameObject);
    }
}
