using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyType1 : MonoBehaviour
{

    [SerializeField]
    private float _speed = 4.0f;

    private Player _player;

    [SerializeField]
    private GameObject _enemyDeath;

    [SerializeField]
    private GameObject[] powerUps;


    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("Player_lv1").GetComponent<Player>();
        if(_player == null)
        {
            Debug.Log("The player is Null");
        }
    }

    // Update is called once per frame
    void Update()
    {
        EnemyMovement();
    }

    private void OnTriggerEnter2D(Collider2D other)
    { 
        if(other.tag == "Player")
        {
            Player player = other.transform.GetComponent<Player>();
            if(player != null)
            {
                if (!player.getIsShieldActive())
                {
                    other.transform.GetComponent<Player>().PlayerDamage();  //Damage player

                }
                Destroy(this.gameObject);       //Destroy enemy
                _enemyDeath.SetActive(true);
                Instantiate(_enemyDeath, transform.position, Quaternion.identity);
            }

            //Destroy(other.gameObject);
        }
        else if (other.tag == "Laser_1")
        {
            var randomPU = Random.Range(0, 100);
            Destroy(other.gameObject); //Destroy Bullet
            Destroy(this.gameObject); //Destroy enemy
            Debug.Log(randomPU);

            //Spawn Random Powerups
            if (randomPU >= 0 && randomPU <= 10)
            {
                Instantiate(powerUps[0], transform.position, Quaternion.identity);
            }
            else if (randomPU >= 20 && randomPU <= 40)
            {
                Instantiate(powerUps[1], transform.position, Quaternion.identity);
            }
            if (randomPU >= 80 && randomPU <= 100)
            {
                Instantiate(powerUps[2], transform.position, Quaternion.identity);
            }
            if(_player != null) 
            {
                _player.AddScore(10);
            }
            _enemyDeath.SetActive(true);
            Instantiate(_enemyDeath, transform.position, Quaternion.identity);
        }

    }

    void EnemyMovement()
    {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);

        var positionRdn = Random.Range(-7.5f, 7.5f);

        if (transform.position.y < -5f)
        {

            transform.position = new Vector3(positionRdn, 5, 0);
        }
    }
}
