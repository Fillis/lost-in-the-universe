using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroide : MonoBehaviour
{

    [SerializeField]
    private float _speed;

    [SerializeField]
    private int _asteroidId;

    [SerializeField]
    private GameObject _asteroidDeath;
    [SerializeField]
    private float _distance;

    private Rigidbody2D rb;
    private bool _isAvtive;
    private Vector2 moveDirection;

    private Player _player;
    //private Vector3 _positionPlayer;

    // Start is called before the first frame update

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
            _isAvtive = false;

            _speed = Random.Range(1.0f, 3.0f);

            _player = GameObject.Find("Player_lv1").GetComponent<Player>();
            if (_player == null)
            {
                Debug.Log("The player is Null");
            }

        /*
            _positionPlayer = _player.transform.position.normalized;
        */
    }

    // Update is called once per frame
    void Update() {

        AsteroidMovement();

        CheckView();
    }

    public void AsteroidMovement()
    {

        if(!_isAvtive) 
        {
            var x = Mathf.Pow((transform.position.x - _player.gameObject.transform.position.x), 2);
            var y = Mathf.Pow((transform.position.y - _player.gameObject.transform.position.y), 2);

            _distance = Mathf.Sqrt(x + y);

            Vector3 direction = (transform.position - _player.gameObject.transform.position).normalized;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            rb.rotation = angle;
            moveDirection = direction;
            rb.velocity = new Vector2(- moveDirection.x, - moveDirection.y) * _speed;
        }
        else
        {
            var direction = new Vector3(1.0f, -1.0f, 0f);
            transform.Translate(direction * _speed * Time.deltaTime);
        }

        if(_distance < 1.5f)
        {
            _isAvtive = true;
        }

        //transform.Translate(_player.transform.position * _speed * Time.deltaTime);
        //transform.Translate(_positionPlayer * _speed * Time.deltaTime);
    }

    public void CheckView()
    {
        if(this.gameObject.transform.position.y < -6)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Player player = other.transform.GetComponent<Player>();
            if (player != null)
            {
                if (!player.getIsShieldActive())
                {
                    other.transform.GetComponent<Player>().PlayerDamage();  //Damage player

                }
                Destroy(this.gameObject);       //Destroy enemy
                _asteroidDeath.SetActive(true);
                Instantiate(_asteroidDeath, transform.position, Quaternion.identity);
            }

            //Destroy(other.gameObject);
        }
        else if (other.tag == "Laser_1")
        {
            var randomPU = Random.Range(0, 100);
            Destroy(other.gameObject); //Destroy Bullet
            Destroy(this.gameObject); 
            Debug.Log(randomPU);

            if (_player != null)
            {
                _player.AddScore(10);
            }
            _asteroidDeath.SetActive(true);
            Instantiate(_asteroidDeath, transform.position, Quaternion.identity);
        }

    }

}
