using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _enemy1Prefab;

    [SerializeField]
    private GameObject[] _asteroids;

    [SerializeField]
    private GameObject _enemyContainer;

    [SerializeField]
    private GameObject _player;

    private bool _isPlayerAlive = true;

    // Start is called before the first frame update
    void Start()
    {

        StartCoroutine(SpawnRoutine());
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator SpawnRoutine()
    {

        while (_isPlayerAlive)
        {
            Vector3 posToSpawn = new Vector3(Random.Range(-7.5f, 7.5f), 6, 0);
            GameObject newEnemy = Instantiate(_enemy1Prefab, posToSpawn, Quaternion.identity);
            newEnemy.transform.parent = _enemyContainer.transform;
            yield return new WaitForSeconds (3.0f); //wait 1 frame
            StartCoroutine(SpawnRoutineAsteroide());
        }
    }

    public void OnPlayerDeath()
    {
        _isPlayerAlive = false;
        StartCoroutine(ClearScene());
    }

    IEnumerator SpawnRoutineAsteroide()
    {
        Vector3 posToSpawn = new Vector3(-7.5f, 5, 0);
        GameObject newEnemy = Instantiate(_asteroids[Random.Range(0, 2)], posToSpawn, Quaternion.identity);
        newEnemy.transform.parent = _enemyContainer.transform;
        Debug.Log("Asteroid spawned");
        yield return new WaitForSeconds(3.0f); //wait 1 frame
    }

    IEnumerator ClearScene()
    {
        yield return new WaitForSeconds(5.0f); //wait 1 frame
        foreach (Transform child in _enemyContainer.transform)
        {
            Destroy(child.gameObject);
        }
    }
}
